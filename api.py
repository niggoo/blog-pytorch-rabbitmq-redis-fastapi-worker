from typing import Union
from fastapi import FastAPI, UploadFile
from pika import BasicProperties
from pydantic import BaseModel
from PIL import Image
import numpy as np
import pickle
import uuid
from bridge import redis_client, rabbitmq_client

api = FastAPI()


class PendingClassificationDTO(BaseModel):
    inference_id: str


class ClassificationResultDTO(BaseModel):
    predicted_class: int


@api.post('/classify', response_model=PendingClassificationDTO, status_code=202)
async def classify(file: UploadFile):
    im = Image.open(file.file)
    image_data = np.array(im)

    inference_id = str(uuid.uuid4())

    rabbitmq_client.basic_publish(
        exchange='',  # default exchange
        routing_key='mnist_inference_queue',
        body=pickle.dumps(image_data),
        properties=BasicProperties(headers={'inference_id': inference_id})
    )

    return PendingClassificationDTO(inference_id=inference_id)


@api.get('/result/{inference_id}', status_code=200,
         response_model=Union[ClassificationResultDTO, PendingClassificationDTO])
async def classification_result(inference_id):
    if not redis_client.exists(inference_id):
        return PendingClassificationDTO(inference_id=inference_id)

    result = redis_client.get(inference_id)
    return ClassificationResultDTO(predicted_class=result)
